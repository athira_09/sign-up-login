from django.db import models
# Create your models here.
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _


class CustomUser(AbstractUser):
    bio = models.TextField(max_length=100)
    phone_number = models.IntegerField(null=True, unique=True)
    email = models.EmailField(_('email address'), unique=True)

    def __str__(self):
        return self.email
