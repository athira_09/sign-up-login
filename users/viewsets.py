from rest_framework import viewsets

from users.models import CustomUser
from users.serializers import UserSerializer


class RegistrationViewSet(viewsets.ModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer

